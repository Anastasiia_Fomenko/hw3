import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User, UserService } from 'src/app/services/user.service';
import { GenderPipe } from 'src/app/pipe/gender.pipe';

@Component({
  selector: 'lab-js-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [GenderPipe]
})
export class UserComponent implements OnInit {
  public user$: Observable<User>;
  public constructor(
    private readonly userService: UserService,
  ) { }
  
  public ngOnInit(): void {
    this.user$ = this.userService.getCurrentUser$();
  }

}
